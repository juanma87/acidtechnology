import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

// Routes
import { AppRoutingModule } from './app-routing.module';

// Components
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/shared/nav-bar/nav-bar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { HeroHomeComponent } from './components/home/hero-home/hero-home.component';
import { AboutComponent } from './components/home/about/about.component';
import { MyToolsComponent } from './components/my-tools/my-tools.component';
import { HeroMyToolsComponent } from './components/my-tools/hero-my-tools/hero-my-tools.component';
import { BodyMyToolsComponent } from './components/my-tools/body-my-tools/body-my-tools.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    FooterComponent,
    HomeComponent,
    HeroHomeComponent,
    AboutComponent,
    MyToolsComponent,
    HeroMyToolsComponent,
    BodyMyToolsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
