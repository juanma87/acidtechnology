import { Component, OnInit } from '@angular/core';
import { ToolsService } from '../../../services/tools.service';

@Component({
  selector: 'app-body-my-tools',
  templateUrl: './body-my-tools.component.html',
  styleUrls: ['./body-my-tools.component.scss']
})
export class BodyMyToolsComponent implements OnInit {

  constructor( private _tools: ToolsService) { }

  ngOnInit() {
  }

}
