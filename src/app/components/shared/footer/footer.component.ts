import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public dateFooter: any;

  constructor() { }

  ngOnInit() {

    this.dateFooter = new Date().getFullYear();

  }

}
