import { Component, OnInit } from '@angular/core';
import { NavService } from '../../../services/nav.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor( private _nav: NavService ) { }

  ngOnInit() {
  }

  btn_menu() {
    document.querySelector('#btn-menu').addEventListener('click', function() {
      this.classList.toggle('show');
      document.querySelector('#menu-mobile').classList.toggle('show');
    });
  }
}
