import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  logos: any;

  constructor( private _http: HttpClient ) {

    this._http.get('https://jmmcdesign-6a835.firebaseio.com/nav-bar-footer.json').subscribe( logos => {

      this.logos = logos;
      // console.log( tools );
    });
  }
}
