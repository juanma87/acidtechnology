import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {

  tools: any;

  constructor( private _http: HttpClient ) {

    this._http.get('https://jmmcdesign-6a835.firebaseio.com/tools.json').subscribe( tools => {

      this.tools = tools;
      // console.log( tools );
    });
  }
}
